
/**
 * Module dependencies.
 */

var express = require('express')
  , http = require('http')
  , path = require('path')
  , request = require('request')
    ;



var app = express();

// all environments
app.set('port', process.env.PORT || 3001);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.engine('html', require('ejs').renderFile);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get("/", function(req, res){

    return res.render("index.html");
});

app.post('/proxy', function(req, res){
    var _url = req.url.split('?')[1];

    request({
            method: "POST",
            uri: _url,
            json: true,
            form: req.body
        },
        function(error, response, body){
            if(!error && response.statusCode === 200){
                return res.json(body)
            }else{
                return res.json({error: "request"})
            }
    });

});


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
